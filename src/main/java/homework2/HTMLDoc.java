package homework2;

import java.io.Serializable;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class HTMLDoc implements Serializable {
	
	private String id = "";
	public String content = "";
	private String urlContent = "";


	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
		//this.id = content.substring(content.indexOf("URL:"), content.indexOf("Date:")).trim();
		//this.urlContent = this.extractURLS(content);
		
	}
	
	private String extractURLS(String strContent){
		String urlContentString = strContent;
		Pattern p = Pattern.compile("\\(?\\b(http://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]");
		Matcher m = p.matcher(urlContentString);
		while(m.find()) {
			String urlStr = m.group();
			if (urlStr.startsWith("(") && urlStr.endsWith(")")){
				urlStr = urlStr.substring(1, urlStr.length() - 1);
			}
			urlContentString = urlContentString.concat(urlStr+"\n");
		}
		
		return urlContentString;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		

	}

	/**
	 * @return the urlContent
	 */
	public String getUrlContent() {
		return urlContent;
	}

	/**
	 * @param urlContent the urlContent to set
	 */
	public void setUrlContent(String urlContent) {
		this.urlContent = urlContent;
	}

}
