package preprocess;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.function.FlatMapFunction;

public class ParseHTMLDoc implements FlatMapFunction<Iterator<String>, String>{

	String delimiter = "==P=>>>>=i===<<<<=T===>=A===<=!Junghoo!==>";

	public Iterable<String> call(Iterator<String> lines) throws Exception {
		// TODO Auto-generated method stub
		ArrayList<String>htmlDocs = new ArrayList<String>();
		StringBuilder sb = new StringBuilder(""); 
		while(lines.hasNext()){
			String line = lines.next();
			if(line.contains(delimiter)){
				htmlDocs.add(sb.toString());
				sb = new StringBuilder("");
			}
			else{
				if(line !=null) sb.append(line);
			}
		}
		return htmlDocs;
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
