package preprocess;

import java.io.File;
import java.util.List;
import java.util.Scanner;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.mllib.linalg.Matrices;
import org.apache.spark.mllib.linalg.Matrix;
import scala.Tuple2;


public class ParseHDFSFile {

	public ParseHDFSFile() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub
		
		SparkConf sparkConf = new SparkConf().setMaster("local[2]").setAppName("Preprocess");
		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);
		
		//JavaRDD<String> input = sparkContext.textFile("hdfs://node1:54310/webdataset");
		JavaRDD<String> input = sparkContext.textFile("/Users/mfamith/Desktop/webdataset.txt");
		
		JavaRDD<String> htmlRDD = input.mapPartitions(new ParseHTMLDoc());

		JavaPairRDD<String, String>contentPairs = htmlRDD.mapToPair(new ExtractContent());
		
		//Remove elements with blank keys, robot.txt pages, and non html urls
		JavaPairRDD<String, String>filteredPairs = contentPairs.filter(new FilterEmptyKeys());
		
		//Unique URLS
		JavaRDD<String>uniqueURLS = filteredPairs.flatMap(new RetrieveUniqueURLS());
		
		//URLS as Graphs
		JavaRDD<String>urlGraph = filteredPairs.flatMap(new MakeGraph());
		
		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a graph file: ");
		String outputPath = scanner.next();
		urlGraph.saveAsTextFile(outputPath);
		
		System.out.print("Enter an output file for urls: ");
		String outputURLs = scanner.next();
		uniqueURLS.saveAsTextFile(outputURLs);
		
		scanner.close();
		System.out.println("Done");
		Thread.sleep(4000);

		sparkContext.close();
	}

}
