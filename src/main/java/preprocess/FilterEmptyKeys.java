package preprocess;

import java.util.regex.Pattern;

import org.apache.spark.api.java.function.Function;

import scala.Tuple2;

public class FilterEmptyKeys implements Function<Tuple2<String, String>, Boolean> {

	public FilterEmptyKeys() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Boolean call(Tuple2<String, String> keyValue) throws Exception {
		// TODO Auto-generated method stub

	
		return ( (keyValue._1().length()>4)&&
				(!keyValue._1.contains("robots.txt"))&&
						(!keyValue._1.contains(".gif")) 
				);
	}

}
