package preprocess;

import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;

import org.apache.spark.api.java.function.FlatMapFunction;

import scala.Tuple2;

public class RetrieveUniqueURLS implements FlatMapFunction<Tuple2<String, String>, String> {

	public RetrieveUniqueURLS() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Iterable<String> call(Tuple2<String, String> tuple) throws Exception {
		// TODO Auto-generated method stub

		String[] urlValues = tuple._2().split(";");
		
		
		ArrayList<String>urls = new ArrayList<String>(Arrays.asList(urlValues));
		Iterator<String>istr = urls.iterator();
		while(istr.hasNext()){
			String url = istr.next();
			
			//remove unwanted urls
			
			
			if(url.contains(".png") || url.contains(".gif") || url.contains(".jpeg") || url.contains(".jpg")|| url.contains(".pdf"))
			{
				istr.remove();
			}
			
			
			if(url.equals("amp")){
				istr.remove();
			}
			
	
			
		}
		
		//iteriate again to remove non urls 
		Iterator<String>s_istr = urls.iterator();
		while(s_istr.hasNext()){
			String url = s_istr.next();
			if((url.indexOf("http")==-1)){
				s_istr.remove();
			}
		}
		
		urls.add(tuple._1());
		
		
		return urls;
	}

}
