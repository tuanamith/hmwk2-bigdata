package preprocess;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.spark.api.java.function.PairFunction;

import scala.Tuple2;

public class ExtractContent implements PairFunction<String, String, String>{

	String regex = "\\(?\\b(http://|https://|www[.])[-A-Za-z0-9+&amp;@#/%?=~_()|!:,.;]*[-A-Za-z0-9+&amp;@#/%=~_()|]";

	public ExtractContent() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Tuple2<String, String> call(String hDoc) throws Exception {
		// TODO Auto-generated method stub

		//Get the key based on the first occurrence of "URL:" and "Date:" to capture the url of the document
		StringBuilder x = new StringBuilder(hDoc); 
		String docKey ="";
		
		if(x.indexOf("URL:")>-1 && x.indexOf("Date:")>-1){
			docKey = x.substring(x.indexOf("URL:"), x.indexOf("Date:")).trim();
			docKey = docKey.replace("URL:", "").trim();
		}

		// TODO Auto-generated catch block


		//Get the urls from body of String
		String urlContent = "";
		Pattern p = Pattern.compile(regex);
		Matcher m = p.matcher(x); 
		while(m.find()) {
			String urlStr = m.group();
			if (urlStr.startsWith("(") && urlStr.endsWith(")")){
				urlStr = urlStr.substring(1, urlStr.length() - 1);
			}
			
			if(!urlStr.substring(urlStr.length() - 4).equals("Date")){
				urlContent = urlContent.concat(urlStr+";");
			}
			
			
			
		}
		return new Tuple2<String, String>(docKey, urlContent);


	}


}
