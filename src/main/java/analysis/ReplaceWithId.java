package analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.apache.spark.api.java.function.Function;

public class ReplaceWithId implements Function<String, String> {
	

	private Map<String,Integer>urls = new HashMap<String,Integer>();


	public ReplaceWithId(List<String> input) {
		// TODO Auto-generated constructor stub

		for(String item:input){
		
			urls.put(item, 0);
			
		}
		
		//assign unique id to each unique url
		int counter =0;
		
		for(Entry<String, Integer> entry : urls.entrySet()){
			entry.setValue(counter);
			counter++;
		}
		
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public String call(String oldURL) throws Exception {
		// TODO Auto-generated method stub
		
		String newURL = "";
		String[] splitString = oldURL.split("\\s\\|\\s");
		if(splitString.length>1){
			

			if(urls.get(splitString[0]) != null){
				newURL = newURL.concat(urls.get(splitString[0]) + " | ");
				
			}
			

			if(urls.get(splitString[1]) != null){
				newURL = newURL.concat(urls.get(splitString[1])+"");
				
			}
			
		}
		return newURL;

		
	}

}
