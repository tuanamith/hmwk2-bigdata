package analysis;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import org.apache.spark.api.java.function.PairFlatMapFunction;
import org.apache.spark.api.java.function.PairFunction;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;

import scala.Tuple2;

public class FindPopular implements PairFunction <String, String, Integer> {
	
	private Table<String, String, Integer> matrix = HashBasedTable.create();;

	public FindPopular(Table<String, String, Integer> matrix ) {
		// TODO Auto-generated constructor stub
		this.matrix.putAll(matrix);
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	public Tuple2<String, Integer> call(String arg0) throws Exception {
		// TODO Auto-generated method stub
		int counter =0;
		Iterator<Entry<String,Integer>> iterator = matrix.column(arg0).entrySet().iterator();
		while (iterator.hasNext()) {
			Map.Entry<String,Integer> entry = (Map.Entry<String,Integer>) iterator.next();
			counter++;
		}

		
		return new Tuple2<String,Integer>(arg0, counter);
	}


}
