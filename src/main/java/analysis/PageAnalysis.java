package analysis;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Scanner;
import java.util.Set;

import org.apache.spark.Accumulator;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.api.java.function.Function2;
import org.apache.spark.api.java.function.VoidFunction;

import com.google.common.collect.HashBasedTable;
import com.google.common.collect.Table;
import com.google.common.collect.Table.Cell;

import preprocess.ExtractContent;
import preprocess.ParseHTMLDoc;
import scala.Tuple2;

public class PageAnalysis {

	public PageAnalysis() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) throws InterruptedException {
		// TODO Auto-generated method stub

		SparkConf sparkConf = new SparkConf().setMaster("local[2]").setAppName("PageAnalysis");
		JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);

		Scanner scanner = new Scanner(System.in);
		System.out.print("Enter a graph location/file (Example: graphURLs): ");
		String graphOutput = scanner.next();

		System.out.print("Enter a url list location/file (Example: cleanURLs) : ");
		String cleanOutput = scanner.next();

		JavaRDD<String> urlGraphs = sparkContext.textFile(graphOutput);
		JavaRDD<String> urls = sparkContext.textFile(cleanOutput);

		//For numeric graphs
		JavaRDD<String>urlGraphsNumProcess = urlGraphs.map(new ReplaceWithId(urls.collect()));
		JavaRDD<String>urlGraphsNumeric = urlGraphsNumProcess.filter(new FilterOrphanSites());

		//save numeric graph if needed; uncomment if not needed
		//urlGraphsNumeric.saveAsTextFile("urlGraphNumericArrow");


		/**** ANALYSIS PART****/
		Table<String, String, Integer> matrix = HashBasedTable.create();
		Integer maxOutDegree = 1;
		Integer minOutDegree = 1;
		int counter =0;
		int mostPopular =0;


		//create master matrix for reference
		for(String url:urlGraphs.collect()){

			String[] splitString = url.split("\\s\\|\\s");
			if(splitString.length>1)
			{

				if(matrix.contains(splitString[0].trim(), splitString[1].trim())){

					int count = matrix.get(splitString[0].trim(), splitString[1].trim());
					count++;
					matrix.put(splitString[0].trim(), splitString[1].trim(), count);
				}
				else{

					matrix.put(splitString[0].trim(), splitString[1].trim(), 1);
				}
			}

		}

		/****MIN and MAX Degrees****/
		/*******AND *******/
		/***** HISTOGRAMS *******/

		JavaRDD<Integer>matrixDegreesRDD = sparkContext.parallelize(new ArrayList<Integer>(matrix.values())); 
		maxOutDegree = matrixDegreesRDD.top(1).get(0);
		minOutDegree = matrixDegreesRDD.reduce(
				new Function2<Integer, Integer, Integer>(){

					public Integer call(Integer x, Integer y) throws Exception {
						// TODO Auto-generated method stub
						if(x<y){
							return x;
						}
						else{
							return y;
						}

					}
					
				}
				);
		Map<Integer,Long>histogramValues =matrixDegreesRDD.countByValue();
		
		/****MOST POPULAR VERTEX****/
		JavaRDD<String>outVertexRDD = sparkContext.parallelize(new ArrayList<String>(matrix.columnKeySet()));
		
		JavaPairRDD<String,Integer>popularRDD = outVertexRDD.mapToPair(new FindPopular(matrix));
		
		Tuple2<String, Integer>popularVertex= popularRDD.reduce(
				new Function2<Tuple2<String,Integer>,Tuple2<String,Integer>,Tuple2<String,Integer>>(){
					public Tuple2<String, Integer> call(
							Tuple2<String, Integer> arg0,
							Tuple2<String, Integer> arg1) throws Exception {
						// TODO Auto-generated method stub
						
						if(arg0._2()>arg1._2()){
							return arg0;
						}
						else{
							return arg1;
						}
						
					}
					
				}
				);
		
		
		/****MOST OUTGOING VERTEX****/
		JavaRDD<String>cellsRDD = sparkContext.parallelize(cellsForParallel(matrix.cellSet()));
		String mostOutgoingData = cellsRDD.reduce(
				new Function2<String, String, String>(){

					public String call(String arg0, String arg1)
							throws Exception {
						// TODO Auto-generated method stub
						String[] first = arg0.split("\\)=");
						String[] second = arg1.split("\\)=");
						int intFirst =Integer.parseInt(first[1]);
						int intSecond =Integer.parseInt(second[1]);
						
						if(intFirst>intSecond){
							return arg0;
						}
						else{
							return arg1;
						}
						
		
					}
					
				}
				);
		
		String mostOutgoingVertex = mostOutgoingData.substring(1, mostOutgoingData.indexOf(","));
		String numberOutgoingDegree = mostOutgoingData.substring(mostOutgoingData.lastIndexOf("=")+1, mostOutgoingData.length());
		
		
		System.out.println("MIN and MAX degree");
		System.out.println("------------------");
		System.out.println("Max out degree: " + maxOutDegree);
		System.out.println("Min our degree: " + minOutDegree);
		System.out.println("");
		Thread.sleep(4000);
		
		System.out.println("HISTOGRAM");
		System.out.println("---------");
		for(Map.Entry<Integer, Long>entry: histogramValues.entrySet()){
			System.out.println("Degree: " + entry.getKey() + " - " + entry.getValue());
		}
		System.out.println("");
		Thread.sleep(4000);
		
		System.out.println("POPULAR VERTEX");
		System.out.println("--------------");
		System.out.println(popularVertex._1() + " (" +popularVertex._2()+")");
		System.out.println("");
		Thread.sleep(4000);
		
		System.out.println("OUTGOING VERTEX");
		System.out.println("---------------");
		System.out.println(mostOutgoingVertex + " (" + numberOutgoingDegree +")");
		System.out.println("");
		Thread.sleep(4000);
		
		scanner.close();

	}
	
	private static ArrayList<String> cellsForParallel(Set<Cell<String, String, Integer>> setCells){
		ArrayList<String>cells = new ArrayList<String>(); 
		for (Cell<String, String, Integer> cell: setCells){
			cells.add(cell.toString());
		}
		return cells;
	}

}
