package analysis;

import org.apache.spark.api.java.function.Function;

public class URLToDomainGraph implements Function <String, String>  {

	public URLToDomainGraph() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	private String getDomain(String url){
		String domain=url.toLowerCase();
		
		if(domain.indexOf(".net")>-1){
			domain = domain.substring(0, domain.indexOf(".net")+4);
		}
		else if(domain.indexOf(".com")>-1){
			domain = domain.substring(0, domain.indexOf(".com")+4);
		}
		else if(domain.indexOf(".org")>-1){
			domain = domain.substring(0, domain.indexOf(".org")+4);
		}
		else if(domain.indexOf(".gov")>-1){
			domain = domain.substring(0, domain.indexOf(".gov")+4);
		}
		else if(domain.indexOf(".us")>-1){
			domain = domain.substring(0, domain.indexOf(".us")+3);
		}
		else if(domain.indexOf(".edu")>-1){
			domain = domain.substring(0, domain.indexOf(".edu")+4);
		}
		else if(domain.indexOf(".uk")>-1){
			domain = domain.substring(0, domain.indexOf(".uk")+3);
		}
		
		return domain;
	}

	public String call(String e2e) throws Exception {
		// TODO Auto-generated method stub
		String[]domains = e2e.split("\\s\\|\\s");
		String source = domains[0];
		String destination = domains[1];
		
		source = this.getDomain(source);
		destination = this.getDomain(destination);
		
		String post = source + " | " + destination;
		
		return post;
	}

}
