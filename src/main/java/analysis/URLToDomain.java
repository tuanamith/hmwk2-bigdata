package analysis;

import java.net.URL;
import java.util.regex.Pattern;

import org.apache.spark.api.java.function.Function;

public class URLToDomain implements Function <String, String> {
	

	public URLToDomain() {
		// TODO Auto-generated constructor stub
	}

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

	public String call(String url) throws Exception {
		// TODO Auto-generated method stub
		
		String domain=url.toLowerCase();
		
		if(domain.indexOf(".net")>-1){
			domain = domain.substring(0, domain.indexOf(".net")+4);
		}
		else if(domain.indexOf(".com")>-1){
			domain = domain.substring(0, domain.indexOf(".com")+4);
		}
		else if(domain.indexOf(".org")>-1){
			domain = domain.substring(0, domain.indexOf(".org")+4);
		}
		else if(domain.indexOf(".gov")>-1){
			domain = domain.substring(0, domain.indexOf(".gov")+4);
		}
		else if(domain.indexOf(".us")>-1){
			domain = domain.substring(0, domain.indexOf(".us")+3);
		}
		else if(domain.indexOf(".edu")>-1){
			domain = domain.substring(0, domain.indexOf(".edu")+4);
		}
		else if(domain.indexOf(".uk")>-1){
			domain = domain.substring(0, domain.indexOf(".uk")+3);
		}
		
		//Issues with extended urls like: http://www.csac.counties.org/www.butecounty.net, 
		//http://www.csac.counties.org/mailto:csac_web@blueriverdigital.com
		if((domain.lastIndexOf("/")>6) && (domain.lastIndexOf("/")!=domain.length())){
			domain = domain.substring(0, domain.lastIndexOf("/"));
		}

		
		return domain;
	}

}
